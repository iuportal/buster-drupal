FROM drupal:9.0.7-apache-buster

# Install packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    vi \
    curl \
    bash \
    wget \
    libpng-dev \
    zlib1g-dev \
    git \
    zip \
    libzip-dev \
    unzip 
    
#    php7-mbstring \
#    libpng \
#    openssh \
#    ssmtp \ # No tengo claro si necesitamos esto para enviar correos
#    sendmail \ # No tengo claro si necesitamos esto para enviar correos

# Instalamos imagemagick. Compilo porque no encuentro paquete para buster. 
RUN cd /tmp && wget https://www.imagemagick.org/download/releases/ImageMagick-7.0.10-38.tar.gz && \
    tar xvzf ImageMagick-7.0.10-38.tar.gz && cd ImageMagick-7.0.10-38/ && \
    ./configure && \
    make && \
    make install && \
    ldconfig /usr/local/lib 
#RUN apt-get install --no-install-recommends -y libmagickwand-dev \
#    imagemagick \
#    pecl install imagick
#RUN export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
#    && apt-get update \
#    && apt-get install -y --no-install-recommends \
#        libmagickwand-dev \
#    && rm -rf /var/lib/apt/lists/* \
#    && pecl install imagick-3.4.3 \
#    && docker-php-ext-enable imagick
    
# Revisar esto para usar en lugar de este paquete el de buster mariadb-client
RUN apt-get install --no-install-recommends -y default-mysql-client

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    ln -s /root/.composer/vendor/bin/drush /usr/local/bin/drush

# Install Drush
RUN composer global require drush/drush && \
    composer global update

#Instalamos opcache
RUN docker-php-ext-install opcache
RUN docker-php-ext-enable opcache

# install mysql
RUN docker-php-ext-install mysqli 
RUN docker-php-ext-enable mysqli

# Clean repository
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
